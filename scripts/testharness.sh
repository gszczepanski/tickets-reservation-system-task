#!/bin/bash

# VARIABLES
PORT=9000
APP_VERSION="0.0.1-SNAPSHOT"

# FUNCTIONS
function log {
	printf "\n--- $1 ---\n"
}

function build {
	log "RUNNING MVN BUILD"
	mvn clean package -P dev
	if [ "$?" -ne 0 ]; then
    		log "INVALID BUILD"
    		exit 1
	fi
}

function shutdownServerOnPort {
	log "CLEANING UP PORT"
	kill -9 $(lsof -t -i:$PORT)
}

function runApp {
	log "RUNNING RESERVATION_API"
	java -jar target/ticketarea-reservation-api-$APP_VERSION.jar &
}

function runAutomatedTests {
	sleep 20s
	log "RUNNING AUTOMATED TESTS"
	mvn clean test -P automated-tests
	if [ "$?" -ne 0 ]; then
    		log "AUTOMATED TESTS FAILED"
		shutdownServerOnPort
    		exit 1
	fi
}

function tearDown {
	shutdownServerOnPort
	exit 0
}

# PROCESS
log "STARTING TEST HARNESS BUILD"
build
shutdownServerOnPort
runApp
runAutomatedTests
log "TEST HARNESS FINISHED SUCCESSFULLY"
tearDown


