package com.ticketarea.reservation.api.application.reservation

import com.google.common.collect.Sets
import com.ticketarea.reservation.api.application.shared.DomainRegistry
import com.ticketarea.reservation.api.application.ticket.Ticket
import com.ticketarea.reservation.api.application.ticket.TicketRepository
import spock.lang.Specification

import static com.ticketarea.reservation.api.application.reservation.ReservedTickets.ReservedTicket
import static com.ticketarea.reservation.api.application.reservation.ReservedTickets.createForReservation

class ReservedTicketsSpec extends Specification implements ReservationObjectMother {

    DomainRegistry registry = Stub()
    TicketRepository ticketRepository = Stub()

    def "should create ReservedTickets with valid id"() {
        when:
        ReservedTickets tickets = createForReservation()

        then:
        tickets.id != null
    }

    def "should add Ticket with reservation date time to collection of reserved tickets"() {
        given:
        ReservedTickets tickets = createForReservation()
        Ticket ticket = standardFreeTicket()
        ReservationPointInTime reservationTime = oneMinuteAgoPointInTime()

        when:
        tickets.addToReservedTickets(ticket, reservationTime)

        then:
        ReservedTicket reservedTicket = new ArrayList<>(tickets.reservedTickets).get(0)

        reservedTicket.parent == tickets
        reservedTicket.ticketId == ticket.getId()
        reservedTicket.reservationDate == reservationTime.dateTime
    }

    def "should throw IllegalStateException when tickets added size are greater than 8"() {
        given:
        ReservedTickets tickets = createForReservation()
        0.upto(8, {
            tickets.addToReservedTickets(standardFreeTicket(), oneMinuteAgoPointInTime())
        })

        when:
        tickets.addToReservedTickets(standardFreeTicket(), oneMinuteAgoPointInTime())

        then:
        thrown IllegalStateException
    }

    def "should release found tickets on releaseAllTickets"() {
        given:
        ReservedTickets tickets = createForReservation()
        Ticket firstTicket = standardReservedTicket()
        Ticket secondTicket = standardReservedTicket()

        tickets.addToReservedTickets(firstTicket, oneMinuteAgoPointInTime())
        tickets.addToReservedTickets(secondTicket, oneMinuteAgoPointInTime())

        registry.ticketRepository() >> ticketRepository
        ticketRepository.findByIds(_) >> Sets.newHashSet(firstTicket, secondTicket)

        when:
        tickets.releaseAllTickets(registry)

        then:
        firstTicket.isFree() == true
        secondTicket.isFree() == true
        tickets.reservedTickets.isEmpty() == true
    }
}
