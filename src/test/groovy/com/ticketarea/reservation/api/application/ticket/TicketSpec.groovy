package com.ticketarea.reservation.api.application.ticket

import com.ticketarea.reservation.api.application.user.UserId
import spock.lang.Specification

import javax.validation.constraints.Null

import static com.ticketarea.reservation.api.application.ticket.Ticket.TicketState.*

class TicketSpec extends Specification implements TicketObjectMother {

    def "should created ticket has generated id"() {
        when:
        Ticket ticket = new Ticket(seatDetails())

        then:
        ticket.getId() != null
    }

    def "should created ticket be in free state"() {
        when:
        Ticket ticket = new Ticket(seatDetails())

        then:
        ticket.getState() == FREE
    }

    def "should created ticket contains given seat details"() {
        when:
        Ticket ticket = new Ticket(seatDetails())

        then:
        ticket.getDetails() == seatDetails()
    }

    def "should throw exception when seat details object passed to Ticket constructor is NULL"() {
        when:
        new Ticket(null)

        then:
        thrown NullPointerException
    }

    def "should change state from FREE to RESERVED on reserve method"() {
        given:
        Ticket ticket = standardFreeTicket()

        when:
        ticket.reserve(someUserId(), stephen())

        then:
        ticket.state == RESERVED
    }

    def "should create ownership on reserve method"() {
        given:
        Ticket ticket = standardFreeTicket()
        UserId userId = someUserId()

        when:
        ticket.reserve(userId, stephen())

        then:
        ticket.ownership == new Ownership(userId, stephenOwner  ())
    }

    def "should throw Exception when ticket is already reserved on reserve method"() {
        given:
        Ticket reservedTicket =  standardReservedTicket()

        when:
        reservedTicket.reserve(someUserId(), stephen())

        then:
        thrown TicketOperationDeniedException
    }

    def "should throw exception when userId is NULL on reserve method"() {
        when:
        standardFreeTicket().reserve(null, stephen())

        then:
        thrown NullPointerException
    }

    def "should throw exception when owner is NULL on reserve method"() {
        when:
        standardFreeTicket().reserve(someUserId(), null)

        then:
        thrown NullPointerException
    }

    def "should throw exception when ticket is not in RESERVED state during release ticket operation"() {
        when:
        standardFreeTicket().release()

        then:
        thrown TicketOperationDeniedException
    }

    def "should erase ownership on ticket release"() {
        given:
        Ticket ticket = standardReservedTicket()

        when:
        ticket.release()

        then:
        ticket.ownership == null
    }

    def "should set Ticket state to FREE on ticket release"() {
        given:
        Ticket ticket = standardReservedTicket()

        when:
        ticket.release()

        then:
        ticket.state == FREE
    }
}
