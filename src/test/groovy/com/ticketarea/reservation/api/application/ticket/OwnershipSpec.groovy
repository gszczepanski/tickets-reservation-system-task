package com.ticketarea.reservation.api.application.ticket

import com.ticketarea.reservation.api.application.user.UserId
import spock.lang.Specification

class OwnershipSpec extends Specification implements TicketObjectMother {

    def "should create ownership with given Id and owner details"() {
        given:
        UserId userId = UserId.representationOf("123")

        when:
        Ownership ownership = new Ownership(userId, stephenOwner())

        then:
        ownership.getUserId() == userId
        ownership.getOwner() == stephenOwner()
    }

    def "should throw exception when userId is NULL"() {
        when:
        new Ownership(null, stephenOwner())

        then:
        thrown NullPointerException
    }

    def "should throw exception when owner is NULL"() {
        when:
        new Ownership(UserId.representationOf("any"), null)

        then:
        thrown NullPointerException
    }
}
