package com.ticketarea.reservation.api.application.user

import spock.lang.Specification

class UserIdSpec extends Specification {

    def "should create userId from given string"() {
        given:
        String plainId = "0928348"

        when:
        UserId userId = UserId.representationOf(plainId)

        then:
        userId.getValue() == plainId
    }

    def "should throw exception when userId passed to factory method is NULL"() {
        when:
        UserId.representationOf(null)

        then:
        thrown IllegalArgumentException
    }

    def "should throw exception when userId passed to factory method is blank"() {
        when:
        UserId.representationOf("  ")

        then:
        thrown IllegalArgumentException
    }

}
