package com.ticketarea.reservation.api.application.ticket

import com.ticketarea.reservation.api.application.shared.IdGenerator
import com.ticketarea.reservation.api.application.shared.Person
import com.ticketarea.reservation.api.application.user.UserId

import java.time.LocalDate

trait TicketObjectMother {

    SeatDetails seatDetails() {
        return new SeatDetails("A", "12", "3")
    }

    Person stephen() {
        return new Person("Stephen", "Pattherson", LocalDate.of(1982, 06, 27))
    }

    TicketOwner stephenOwner() {
        return new TicketOwner(stephen())
    }

    UserId someUserId() {
        return UserId.representationOf(IdGenerator.generate())
    }

    Ticket standardFreeTicket() {
        return new Ticket(
                seatDetails()
        )
    }

    Ticket standardReservedTicket() {
        Ticket ticket = standardFreeTicket()
        ticket.reserve(someUserId(), stephen())
        return ticket
    }


}