package com.ticketarea.reservation.api.application.reservation

import spock.lang.Specification

import java.time.LocalDateTime

class ReservationPointInTimeSpec extends Specification {

    def "should create point in time with given date time"() {
        given:
        LocalDateTime past = LocalDateTime.of(2002, 12, 12, 11, 00)

        when:
        ReservationPointInTime pointInTime = ReservationPointInTime.happenedAt(past)

        then:
        pointInTime.getDateTime() == past
    }

    def "should throw NPE when date time passed to create point in time is null"() {
        when:
        ReservationPointInTime.happenedAt(null)

        then:
        thrown NullPointerException
    }

    def "should throw IllegalArgumentException when date time passed is future"() {
        given:
        LocalDateTime future = LocalDateTime.of(2050, 12, 12, 11, 00)

        when:
        ReservationPointInTime.happenedAt(future)

        then:
        thrown IllegalArgumentException
    }
}
