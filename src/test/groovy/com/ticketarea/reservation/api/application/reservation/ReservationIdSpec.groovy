package com.ticketarea.reservation.api.application.reservation

import spock.lang.Specification

class ReservationIdSpec extends Specification {

    def "should create unique reservation id each time generate method is called"() {
        when:
        ReservationId reservationA = ReservationId.generate()
        ReservationId reservationB = ReservationId.generate()

        then:
        reservationA != reservationB
    }

    def "should create reservation id from given string"() {
        given:
        String plainId = "6545672"

        when:
        ReservationId reservationIdId = ReservationId.of(plainId)

        then:
        reservationIdId.getValue() == plainId
    }

    def "should throw exception when reservation id passed to factory method is NULL"() {
        when:
        ReservationId.of(null)

        then:
        thrown IllegalArgumentException
    }

    def "should throw exception when reservation id passed to factory method is blank"() {
        when:
        ReservationId.of("  ")

        then:
        thrown IllegalArgumentException
    }

}
