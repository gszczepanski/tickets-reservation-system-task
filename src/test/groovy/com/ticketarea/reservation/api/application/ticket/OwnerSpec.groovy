package com.ticketarea.reservation.api.application.ticket

import spock.lang.Specification

class OwnerSpec extends Specification implements TicketObjectMother {

    def "should create Owner with given person data"() {
        when:
        TicketOwner owner = new TicketOwner(
                stephen()
        )

        then:
        owner.getFirstName() == stephen().getFirstName()
        owner.getLastName() == stephen().getLastName()
        owner.getDateOfBirth() == stephen().getDateOfBirth()
    }

    def "should throw exception when person is null"() {
        when:
        new TicketOwner(null)

        then:
        thrown NullPointerException
    }

}
