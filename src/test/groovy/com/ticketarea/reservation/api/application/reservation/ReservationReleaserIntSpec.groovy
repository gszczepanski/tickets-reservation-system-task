package com.ticketarea.reservation.api.application.reservation

import com.ticketarea.reservation.api.ReservationApiConfig
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification
import spock.lang.Unroll

import static com.ticketarea.reservation.api.application.reservation.Reservation.ReservationState.EXPIRED

@SpringBootTest(classes = ReservationApiConfig.class)
class ReservationReleaserIntSpec extends Specification implements ReservationObjectMother {

    @Autowired
    ReservationReleaser releaser;

    @Autowired
    ReservationRepository repository;

    @Unroll
    def "should invoke timeValidation on found current Reservation from DB"() {
        given:
        Reservation reservation = reservationForRugbyMatch() // expired date 01-01-2018 12:10
        repository.save(reservation)

        when:
        releaser.releaseReservations()

        then:
        Reservation persistedReservation = repository.findById(reservation.getId()).get()
        persistedReservation.state == EXPIRED
    }
}
