package com.ticketarea.reservation.api.application.reservation

import com.ticketarea.reservation.api.ReservationApiConfig
import com.ticketarea.reservation.api.application.event.EventId
import com.ticketarea.reservation.api.application.ticket.TicketId
import com.ticketarea.reservation.api.application.user.UserId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification
import spock.lang.Unroll

@SpringBootTest(classes = ReservationApiConfig.class)
class ReservationServiceIntSpec extends Specification implements ReservationObjectMother {

    @Autowired
    ReservationService reservationService;

    @Autowired
    ReservationRepository repository;

    @Unroll
    def "should save reservation"() {
        given:
        UserId userId = UserId.representationOf("12")
        EventId eventId = EventId.representationOf("2834")

        when:
        reservationService.createReservation(userId, eventId)

        then:
        Optional<Reservation> reservation = repository.findCurrentByUserIdAndEventId(userId, eventId)

        reservation.isPresent() == true
        reservation.get().userId == userId
        reservation.get().eventId == eventId
    }

    @Unroll
    def "should reserve ticket for reservation"() {
        given:
        UserId userId = UserId.representationOf("13")
        EventId eventId = EventId.representationOf("2834")
        ReservationId reservationId = reservationService.createReservation(userId, eventId)
        TicketId ticketId = TicketId.of("234324")

        ReservationService.ReserveTicketData data = new ReservationService.ReserveTicketData(
                reservationId,
                ticketId,
                oneMinuteAgoPointInTime(),
                janusz()
        )

        when:
        reservationService.reserveTicket(data)

        then:
        Reservation reservation = repository.findById(reservationId).get()
        reservation.reservedTickets.reservedTickets.size() == 1
    }
}
