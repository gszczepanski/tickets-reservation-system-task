package com.ticketarea.reservation.api.application.reservation

import com.ticketarea.reservation.api.application.event.Event
import com.ticketarea.reservation.api.application.event.EventId
import com.ticketarea.reservation.api.application.shared.Person
import com.ticketarea.reservation.api.application.ticket.Ticket
import com.ticketarea.reservation.api.application.ticket.TicketId
import com.ticketarea.reservation.api.application.ticket.TicketObjectMother
import com.ticketarea.reservation.api.application.user.UserId
import org.junit.Test

import java.time.LocalDate
import java.time.LocalDateTime

import static com.ticketarea.reservation.api.application.reservation.Reservation.*
import static com.ticketarea.reservation.api.application.reservation.Reservation.ReservationState.*

trait ReservationObjectMother extends TicketObjectMother {

    TicketId randomTicketId() {
        return TicketId.generate()
    }

    Event rugbyMatchEventFor2019() {
        return new Event(
                EventId.representationOf("2398471209312334"),
                LocalDateTime.of(2018, 01, 01, 11, 00),
                LocalDateTime.of(2019, 01, 01, 13, 00)
        )
    }

    Reservation reservationForRugbyMatch() {
        return new Reservation(
                ReservationId.of("93487459").getValue(),
                UserId.representationOf("234235"),
                rugbyMatchEventFor2019().id,
                reservationForRugbyMatchDateTimeInfo(),
                CURRENT,
                ReservedTickets.createForReservation(),
                0
        )
    }

    ReservationDateTimeInfo reservationForRugbyMatchDateTimeInfo() {
        return new ReservationDateTimeInfo(
                LocalDateTime.of(2018, 01, 01, 12, 00),
                LocalDateTime.of(2018, 01, 01, 12, 10),
                rugbyMatchEventFor2019().salesEndDate
        )
    }

    Reservation infiniteReservationForRugbyMatch() {
        return new Reservation(
                ReservationId.of("93487459").getValue(),
                UserId.representationOf("234235"),
                rugbyMatchEventFor2019().id,
                new ReservationDateTimeInfo(
                        LocalDateTime.MIN,
                        LocalDateTime.MAX,
                        rugbyMatchEventFor2019().salesEndDate
                ),
                CURRENT,
                ReservedTickets.createForReservation(),
                0
        )
    }

    ReservationPointInTime oneMinuteAgoPointInTime() {
        return ReservationPointInTime.happenedAt(LocalDateTime.now().minusMinutes(1))
    }

    Person janusz() {
        return new Person(
                "Janusz",
                "Tracz",
                LocalDate.of(1966, 02, 03)
        )
    }

}