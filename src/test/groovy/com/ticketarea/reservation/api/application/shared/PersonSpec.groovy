package com.ticketarea.reservation.api.application.shared

import com.ticketarea.reservation.api.application.shared.Person
import com.ticketarea.reservation.api.application.ticket.TicketObjectMother
import spock.lang.Specification

class PersonSpec extends Specification implements TicketObjectMother {

    def "should create Person with given params data"() {
        when:
        Person person = new Person(
                stephen().getFirstName(),
                stephen().getLastName(),
                stephen().getDateOfBirth()
        )

        then:
        person == stephen()
    }

    def "should throw exception when birth date is null"() {
        when:
        new Person(
                stephen().getFirstName(),
                stephen().getLastName(),
                null
        )

        then:
        thrown NullPointerException
    }

    def "should throw exception when first name or last name are NULL"(firstName, lastName) {
        when:
        new Person(firstName, lastName, stephen().getDateOfBirth())

        then:
        thrown IllegalArgumentException

        where:
        firstName                | lastName
        null                     | stephen().getLastName()
        stephen().getFirstName() | null
    }

    def "should throw exception when first name or last name are blank"(firstName, lastName) {
        when:
        new Person(firstName, lastName, stephen().getDateOfBirth())

        then:
        thrown IllegalArgumentException

        where:
        firstName                | lastName
        " "                      | stephen().getLastName()
        stephen().getFirstName() | " "
    }
}
