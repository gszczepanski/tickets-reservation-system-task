package com.ticketarea.reservation.api.application.reservation

import com.ticketarea.reservation.api.application.shared.Clock
import com.ticketarea.reservation.api.application.shared.DomainRegistry
import com.ticketarea.reservation.api.application.event.EventId
import com.ticketarea.reservation.api.application.event.EventRepository
import com.ticketarea.reservation.api.application.ticket.Ticket
import com.ticketarea.reservation.api.application.ticket.TicketId
import com.ticketarea.reservation.api.application.ticket.TicketRepository
import com.ticketarea.reservation.api.application.user.UserId
import spock.lang.Specification

import java.time.LocalDateTime

import static com.ticketarea.reservation.api.application.reservation.Reservation.*
import static com.ticketarea.reservation.api.application.reservation.Reservation.ReservationDateTimeInfo.*
import static com.ticketarea.reservation.api.application.reservation.Reservation.ReservationState.*
import static com.ticketarea.reservation.api.application.reservation.Reservation.ReservationState.CURRENT
import static com.ticketarea.reservation.api.application.reservation.Reservation.ReservationState.TERMINATED

class ReservationSpec extends Specification implements ReservationObjectMother {

    DomainRegistry registry = Stub()
    EventRepository eventRepository = Stub()
    ReservationRepository reservationRepository = Stub()
    TicketRepository ticketRepository = Stub()
    Clock clock = Stub()

    Reservation RESERVATION = Stub()

    def "should create Reservation with valid state"() {
        given:
        UserId userId = UserId.representationOf("11211111")
        EventId eventId = rugbyMatchEventFor2019().getId()

        registry.reservationRepository() >> reservationRepository
        reservationRepository.findCurrentByUserIdAndEventId(userId, eventId) >> Optional.empty()

        registry.eventRepository() >> eventRepository
        eventRepository.findById(eventId) >> Optional.of(rugbyMatchEventFor2019())

        registry.clock() >> clock
        clock.whatDateTimeIsIt() >> LocalDateTime.of(2018 as Integer, 06 as Integer, 10 as Integer, 12 as Integer, 30 as Integer)

        when:
        Reservation reservation = create(userId, eventId, registry)

        then:
        reservation.id != null
        reservation.userId == userId
        reservation.eventId == eventId
        reservation.state == CURRENT
    }

    def "should create Reservation with valid date time info"() {
        given:
        UserId userId = UserId.representationOf("11211111")
        EventId eventId = rugbyMatchEventFor2019().getId()
        LocalDateTime startDateTime = LocalDateTime.of(2018 as Integer, 06 as Integer, 10 as Integer, 12 as Integer, 30 as Integer)

        registry.reservationRepository() >> reservationRepository
        reservationRepository.findCurrentByUserIdAndEventId(userId, eventId) >> Optional.empty()

        registry.eventRepository() >> eventRepository
        eventRepository.findById(eventId) >> Optional.of(rugbyMatchEventFor2019())

        registry.clock() >> clock
        clock.whatDateTimeIsIt() >> startDateTime

        when:
        Reservation reservation = create(userId, eventId, registry)

        then:
        reservation.dateTimeInfo.started == startDateTime
        reservation.dateTimeInfo.expires == startDateTime.plusMinutes(RESERVATION_PERIOD_IN_MINUTES)
        reservation.dateTimeInfo.eventSalesEnd == rugbyMatchEventFor2019().salesEndDate
    }

    def "should thrown exception if userId or eventId is missing on Reservation creation"(userId, eventId) {
        when:
        create(userId, eventId, registry)

        then:
        thrown NullPointerException

        where:
        userId                                | eventId
        null                                  | EventId.representationOf("2143")
        UserId.representationOf("2734")  | null
    }

    def "should throw ReservationAlreadyExistException on creation when reservation in CURRENT state already exist for userId and eventId"() {
        given:
        UserId userId = UserId.representationOf("11211111")
        EventId eventId = rugbyMatchEventFor2019().getId()
        registry.reservationRepository() >> reservationRepository
        reservationRepository.findCurrentByUserIdAndEventId(userId, eventId) >> Optional.of(RESERVATION)

        when:
        create(userId, eventId, registry)

        then:
        thrown ReservationAlreadyExistException
    }

    def "should throw IllegalArgumentException on creation if event for given eventId is not found"() {
        given:
        UserId userId = UserId.representationOf("11211111")
        EventId INVALID_EVENT_ID = EventId.representationOf("invalid")

        registry.reservationRepository() >> reservationRepository
        reservationRepository.findCurrentByUserIdAndEventId(userId, INVALID_EVENT_ID) >> Optional.empty()

        registry.eventRepository() >> eventRepository
        eventRepository.findById(INVALID_EVENT_ID) >> Optional.empty()

        when:
        create(userId, INVALID_EVENT_ID, registry)

        then:
        thrown IllegalArgumentException
    }

    def "should throw UnableToCreateReservationForEventException on Reservation creation when event sales date not stared yet"() {
        given:
        UserId userId = UserId.representationOf("11211111")
        EventId eventId = rugbyMatchEventFor2019().getId()

        registry.reservationRepository() >> reservationRepository
        reservationRepository.findCurrentByUserIdAndEventId(userId, eventId) >> Optional.empty()

        registry.eventRepository() >> eventRepository
        eventRepository.findById(eventId) >> Optional.of(rugbyMatchEventFor2019())

        registry.clock() >> clock
        clock.whatDateTimeIsIt() >> LocalDateTime.of(2002 as Integer, 06 as Integer, 10 as Integer, 12 as Integer, 30 as Integer)

        when:
        create(userId, eventId, registry)

        then:
        thrown UnableToCreateReservationForEventException
    }

    def "should throw UnableToCreateReservationForEventException on Reservation creation when current date is after event sales date"() {
        given:
        UserId userId = UserId.representationOf("11211111")
        EventId eventId = rugbyMatchEventFor2019().getId()

        registry.reservationRepository() >> reservationRepository
        reservationRepository.findCurrentByUserIdAndEventId(userId, eventId) >> Optional.empty()

        registry.eventRepository() >> eventRepository
        eventRepository.findById(eventId) >> Optional.of(rugbyMatchEventFor2019())

        registry.clock() >> clock
        clock.whatDateTimeIsIt() >> LocalDateTime.of(2030 as Integer, 06 as Integer, 10 as Integer, 12 as Integer, 30 as Integer)

        when:
        create(userId, eventId, registry)

        then:
        thrown UnableToCreateReservationForEventException
    }

    def "should throw IllegalStateException on timeValidation when state is not CURRENT"(ReservationState state) {
        given:
        Reservation reservation = reservationForRugbyMatch()
        reservation.state = state
        
        when:
        reservation.timeValidation(registry)
        
        then:
        thrown IllegalStateException
        
        where:
        state << [TERMINATED, EXPIRED, FINISHED]
    }

    def "should change state to TERMINATED when reservation should be terminated on timeValidation"() {
        given:
        LocalDateTime now = LocalDateTime.of(2030 as Integer, 06 as Integer, 10 as Integer, 12 as Integer, 30 as Integer)
        Reservation reservation = infiniteReservationForRugbyMatch()

        registry.clock() >> clock
        clock.whatDateTimeIsIt() >>  now

        when:
        reservation.timeValidation(registry)

        then:
        reservation.state == TERMINATED
    }

    def "should set state to EXPIRED when reservation expired on timeValidation"() {
        given:
        Reservation reservation = reservationForRugbyMatch()
        LocalDateTime timeAfterReservationExpiration = reservation.dateTimeInfo.expires.plusSeconds(1)

        registry.clock() >> clock
        clock.whatDateTimeIsIt() >>  timeAfterReservationExpiration

        when:
        reservation.timeValidation(registry)

        then:
        reservation.state == EXPIRED
    }

    def "should stay in CURRENT state when current time is before expiration time on timeValidation"() {
        given:
        Reservation reservation = reservationForRugbyMatch()
        LocalDateTime timeAfterReservationExpiration = reservation.dateTimeInfo.expires.minusSeconds(1)

        registry.clock() >> clock
        clock.whatDateTimeIsIt() >>  timeAfterReservationExpiration

        when:
        reservation.timeValidation(registry)

        then:
        reservation.state == CURRENT
    }

    def "should invoke expiration validation and throw IllegalStateException when Reservation is not in current state during reserveTicket"() {
        given:
        Reservation reservation = reservationForRugbyMatch()
        TicketId ticketId = randomTicketId()

        registry.clock() >> clock
        clock.whatDateTimeIsIt() >> reservationForRugbyMatchDateTimeInfo().expires.plusMinutes(1)

        when:
        reservation.reserveTicket(
                ticketId,
                oneMinuteAgoPointInTime(),
                janusz(),
                registry
        )

        then:
        thrown IllegalStateException
    }

    def "should throw NPE when any argument is null during ticketReservation"(ticketId, reservationTime, person) {
        given:
        Reservation reservation = infiniteReservationForRugbyMatch()
        registry.clock() >> clock
        clock.whatDateTimeIsIt() >> LocalDateTime.now()

        when:
        reservation.reserveTicket(ticketId, reservationTime, person, registry)

        then:
        thrown NullPointerException

        where:
        ticketId          | reservationTime           | person
        null              | oneMinuteAgoPointInTime() | janusz()
        randomTicketId()  | null                      | janusz()
        randomTicketId()  | oneMinuteAgoPointInTime() | null
    }

    def "should throw IllegalArgumentException when ticket with given id not found during reserveTicket"() {
        given:
        Reservation reservation = infiniteReservationForRugbyMatch()
        TicketId ticketId = randomTicketId()

        registry.clock() >> clock
        clock.whatDateTimeIsIt() >> LocalDateTime.now()

        registry.ticketRepository() >> ticketRepository
        ticketRepository.findById(ticketId) >> Optional.empty()


        when:
        reservation.reserveTicket(
                ticketId,
                oneMinuteAgoPointInTime(),
                janusz(),
                registry
        )

        then:
        thrown IllegalArgumentException
    }

    def "should invoke reserve method on found Ticket on reserveTicket"() {
        given:
        Reservation reservation = infiniteReservationForRugbyMatch()

        registry.clock() >> clock
        clock.whatDateTimeIsIt() >> LocalDateTime.now()

        Ticket ticket = Mock()
        TicketId ticketId = randomTicketId()

        registry.ticketRepository() >> ticketRepository
        ticketRepository.findById(ticketId) >> Optional.of(ticket)

        when:
        reservation.reserveTicket(
                ticketId,
                oneMinuteAgoPointInTime(),
                janusz(),
                registry
        )

        then:
        1* ticket.reserve(reservationForRugbyMatch().userId, janusz())
    }

    def "should invoke addToReservedTickets method on ReservedTickets during reserveTicket"() {
        given:
        ReservedTickets reservedTickets = Mock()
        ReservationPointInTime reservationTime = oneMinuteAgoPointInTime()

        Reservation reservation = new Reservation(
                ReservationId.of("93487459").getValue(),
                UserId.representationOf("234235"),
                rugbyMatchEventFor2019().id,
                new ReservationDateTimeInfo(
                        LocalDateTime.MIN,
                        LocalDateTime.MAX,
                        rugbyMatchEventFor2019().salesEndDate
                ),
                CURRENT,
                reservedTickets,
                0
        )

        registry.clock() >> clock
        clock.whatDateTimeIsIt() >> LocalDateTime.now()

        Ticket ticket = Stub()
        TicketId ticketId = randomTicketId()

        registry.ticketRepository() >> ticketRepository
        ticketRepository.findById(ticketId) >> Optional.of(ticket)

        when:
        reservation.reserveTicket(
                ticketId,
                reservationTime,
                janusz(),
                registry
        )

        then:
        1* reservedTickets.addToReservedTickets(ticket, reservationTime)
    }
}
