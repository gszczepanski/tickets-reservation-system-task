package com.ticketarea.reservation.api.application.event

import spock.lang.Specification

class EventIdSpec extends Specification {

    def "should create eventId from given string"() {
        given:
        String plainId = "0928348"

        when:
        EventId eventId = EventId.representationOf(plainId)

        then:
        eventId.getValue() == plainId
    }

    def "should throw exception when eventId passed to factory method is NULL"() {
        when:
        EventId.representationOf(null)

        then:
        thrown IllegalArgumentException
    }

    def "should throw exception when eventId passed to factory method is blank"() {
        when:
        EventId.representationOf("  ")

        then:
        thrown IllegalArgumentException
    }

}
