package com.ticketarea.reservation.api.application.reservation

import com.ticketarea.reservation.api.application.shared.Clock
import com.ticketarea.reservation.api.application.shared.DomainRegistry
import com.ticketarea.reservation.api.application.event.EventId
import com.ticketarea.reservation.api.application.event.EventRepository
import com.ticketarea.reservation.api.application.ticket.TicketId
import com.ticketarea.reservation.api.application.user.UserId
import spock.lang.Specification

import java.time.LocalDateTime

class ReservationServiceSpec extends Specification implements ReservationObjectMother {

    DomainRegistry registry = Stub()
    ReservationRepository repository = Mock()

    EventRepository eventRepository = Stub()
    Clock clock = Stub()

    def "should create Reservation and return it's id"() {
        given:
        ReservationService service = new ReservationService(registry)
        registry.reservationRepository() >> repository

        UserId userId = UserId.representationOf("234235")
        EventId eventId = rugbyMatchEventFor2019().id
        settingInternalReservationCreation(userId, eventId)

        when:
        ReservationId reservationId = service.createReservation(userId, eventId)

        then:
        reservationId != null
    }

    def "should save created Reservation"() {
        given:
        ReservationService service = new ReservationService(registry)
        registry.reservationRepository() >> repository

        UserId userId = UserId.representationOf("234235")
        EventId eventId = rugbyMatchEventFor2019().id
        settingInternalReservationCreation(userId, eventId)

        when:
        service.createReservation(userId, eventId)

        then:
        1 * repository.save({
                Reservation reservation ->
                    reservation.userId == userId
                    reservation.eventId == eventId
        })
    }

    def "should invoke reserve ticket method on Reservation"() {
        given:
        Reservation reservation = Mock()
        TicketId ticketId = standardFreeTicket().id

        ReservationService service = new ReservationService(registry)
        registry.reservationRepository() >> repository
        repository.findById(reservationForRugbyMatch().id) >> Optional.of(reservation)

        ReservationPointInTime pointInTime = oneMinuteAgoPointInTime()

        ReservationService.ReserveTicketData reserveTicketData = new ReservationService.ReserveTicketData(
                reservationForRugbyMatch().id,
                ticketId,
                pointInTime,
                janusz()
        )

        when:
        service.reserveTicket(reserveTicketData)

        then:
        1 * reservation.reserveTicket(
                ticketId,
                pointInTime,
                janusz(),
                registry
        )
    }

    private def "settingInternalReservationCreation"(UserId userId, EventId eventId) {
        registry.reservationRepository() >> repository
        repository.findCurrentByUserIdAndEventId(userId, eventId) >> Optional.empty()

        registry.eventRepository() >> eventRepository
        eventRepository.findById(eventId) >> Optional.of(rugbyMatchEventFor2019())

        registry.clock() >> clock
        clock.whatDateTimeIsIt() >> LocalDateTime.of(2018 as Integer, 06 as Integer, 10 as Integer, 12 as Integer, 30 as Integer)
    }
}
