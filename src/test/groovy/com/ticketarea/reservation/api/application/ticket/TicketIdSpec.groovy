package com.ticketarea.reservation.api.application.ticket

import spock.lang.Specification

class TicketIdSpec extends Specification {

    def "should create unique ticket id each time generate method is called"() {
        when:
        TicketId ticketA = TicketId.generate()
        TicketId ticketB = TicketId.generate()

        then:
        ticketA != ticketB
    }

    def "should create id from given string"() {
        given:
        String plainId = "123234243"

        when:
        TicketId ticketId = TicketId.of(plainId)

        then:
        ticketId.getValue() == plainId
    }

    def "should throw exception when id passed to factory method is NULL"() {
        when:
        TicketId.of(null)

        then:
        thrown IllegalArgumentException
    }

    def "should throw exception when id passed to factory method is blank"() {
        when:
        TicketId.of("  ")

        then:
        thrown IllegalArgumentException
    }
}
