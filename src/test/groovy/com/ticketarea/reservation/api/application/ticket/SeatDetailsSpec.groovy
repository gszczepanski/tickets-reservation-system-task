package com.ticketarea.reservation.api.application.ticket

import spock.lang.Specification

class SeatDetailsSpec extends Specification {

    def "should create seat details with passed args"() {
        given:
        String sector = "A"
        String row = "12"
        String seat = "55b"

        when:
        SeatDetails details = new SeatDetails(sector, row, seat)

        then:
        details.getSector() == sector
        details.getRow() == row
        details.getSeat() == seat
    }

    def "should throw exception if any of arguments is NULL"(sector, row, seat) {
        when:
        new SeatDetails(sector, row, seat)

        then:
        thrown IllegalArgumentException

        where:
        sector | row  | seat
        null   | "14" | "55b"
        "A"    | null | "55b"
        "A"    | "14" | null
    }

    def "should throw exception if any of arguments is blank"(sector, row, seat) {
        when:
        new SeatDetails(sector, row, seat)

        then:
        thrown IllegalArgumentException

        where:
        sector | row  | seat
        " "   | "14" | "55b"
        "A"    | " " | "55b"
        "A"    | "14" | " "
    }
}
