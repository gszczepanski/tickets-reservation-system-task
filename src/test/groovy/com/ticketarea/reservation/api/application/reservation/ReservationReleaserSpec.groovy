package com.ticketarea.reservation.api.application.reservation

import com.google.common.collect.Sets
import com.ticketarea.reservation.api.application.shared.Clock
import com.ticketarea.reservation.api.application.shared.DomainRegistry
import spock.lang.Specification

import java.time.LocalDateTime

import static com.ticketarea.reservation.api.application.reservation.Reservation.ReservationState.EXPIRED

class ReservationReleaserSpec extends Specification implements ReservationObjectMother {

    DomainRegistry domainRegistry = Stub()
    ReservationRepository reservationRepository = Stub()
    Clock clock = Stub()

    def "should find all current Reservations and trigger timeVerification on releaseReservations"() {
        given:
        Reservation reservation = reservationForRugbyMatch()

        domainRegistry.reservationRepository() >> reservationRepository
        reservationRepository.findCurrentReservations() >> Sets.newHashSet(reservation)

        domainRegistry.clock() >> clock
        clock.whatDateTimeIsIt() >> LocalDateTime.MAX

        ReservationReleaser releaser = new ReservationReleaser(domainRegistry)

        when:
        releaser.releaseReservations()

        then:
        reservation.state == EXPIRED
    }
}
