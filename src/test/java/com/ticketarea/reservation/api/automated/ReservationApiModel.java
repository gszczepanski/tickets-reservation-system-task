package com.ticketarea.reservation.api.automated;

import lombok.Builder;
import lombok.Value;

import java.time.LocalDate;
import java.time.LocalDateTime;

class ReservationApiModel {

    public static AddTicketToReservation.AddTicketToReservationBuilder addTicketToReservation() {
        return AddTicketToReservation.builder();
    }

    @Value
    @Builder
    public static class AddTicketToReservation {

        private final LocalDateTime dateTime;
        private final Person person;

    }

    @Value
    public static class Person {

        private final String firstName;
        private final String lastName;
        private final String dateOfBirth;

    }

}
