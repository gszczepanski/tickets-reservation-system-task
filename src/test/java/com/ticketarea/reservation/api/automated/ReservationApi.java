package com.ticketarea.reservation.api.automated;

import com.google.common.collect.ImmutableMap;
import com.ticketarea.reservation.api.automated.ReservationApiModel.AddTicketToReservation;
import lombok.RequiredArgsConstructor;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

import static com.google.common.collect.ImmutableMap.of;
import static java.lang.String.format;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@RequiredArgsConstructor
class ReservationApi {

    private final String serverURL;
    private final String apiVersion;

    ResponseEntity<String> createReservation(String userId, String eventId) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(
                url("/reservation"),
                POST,
                requestBody(of("userId", userId, "eventId", eventId)),
                String.class);
    }

    ResponseEntity<Void> addTicketToReservation(String reservationId, String ticketId, AddTicketToReservation addTicketToReservation) {
        RestTemplate restTemplate = new RestTemplate();
        String uri = format("/reservation/%s/ticket/%s", reservationId, ticketId);
        return restTemplate.exchange(
                url(uri),
                POST,
                new HttpEntity<>(addTicketToReservation, headers()),
                Void.class);
    }

    private HttpEntity<String> requestBody(Map<String, Object> params) {
        return new HttpEntity<>(
                json(params),
                headers()
        );
    }

    private String json(Map<String, Object> params) {
        JSONObject json = new JSONObject();
        params.forEach((key, value) -> {
            try {
                json.put(key, value);
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        });
        return json.toString();
    }

    private HttpHeaders headers() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(APPLICATION_JSON);
        return headers;
    }

    private String url(String path, String... uriVariables) {
        String formattedPath = format(path, uriVariables);
        return format("%s/api/%s%s", serverURL, apiVersion, formattedPath);
    }

}
