package com.ticketarea.reservation.api.automated;

import com.ticketarea.reservation.api.application.shared.IdGenerator;
import org.junit.Test;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;

import static com.ticketarea.reservation.api.automated.ReservationApiModel.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.springframework.http.HttpStatus.CREATED;

public class ReservationFlowAutomatedTest {

    ReservationApi api = new ReservationApi("http://localhost:9000", "v1.0");

    @Test
    public void shouldReservationBeCreatedForUser() {
        ResponseEntity<String> createReservationResponse = api.createReservation(randomId(), randomId());

        assertThat(createReservationResponse.getStatusCode().is2xxSuccessful()).isTrue();
        assertThat(createReservationResponse.getBody()).isNotNull();
    }

    @Test
    public void shouldUniqueReservationBeCreatedForUser() {
        String eventId = "43545938";

        ResponseEntity<String> firstReservationResponse = api.createReservation(randomId(), eventId);
        ResponseEntity<String> secondReservationResponse = api.createReservation(randomId(), eventId);

        assertThat(firstReservationResponse.getBody()).isNotEqualTo(secondReservationResponse.getBody());
    }

    @Test
    public void shouldReceive409WhenReservationForUserAndEventAlreadyExists() {
        String eventId = "43545938";
        String userId = randomId();

        api.createReservation(userId, eventId);

        assertThatThrownBy(() -> api.createReservation(userId, eventId))
                    .hasMessage("409 null");
    }

    //@Test
    public void shouldReservationExpireAfter10MinutesAndAnotherReservationCanBeApplied() {
        String eventId = "43545938";
        String userId = randomId();

        api.createReservation(userId, eventId);
        waitForSeconds(600);
        ResponseEntity<String> createReservationResponse = api.createReservation(userId, eventId);

        assertThat(createReservationResponse.getStatusCode().is2xxSuccessful()).isTrue();
        assertThat(createReservationResponse.getBody()).isNotNull();
    }

    @Test
    public void shouldAddTicketToReservationReturnSuccessStatusCode() {
        String userId = randomId();
        String ticketId = randomId();
        String reservationId = api.createReservation(userId, randomId()).getBody();

        AddTicketToReservation addTicketBody = addTicketToReservation()
                .dateTime(LocalDateTime.now())
                .person(new Person(
                        "David",
                        "Murray",
                        "1972-05-20"
                ))
                .build();

        ResponseEntity<Void> response = api.addTicketToReservation(reservationId, ticketId, addTicketBody);

        assertThat(response.getStatusCode()).isEqualTo(CREATED);
    }

    private void waitForSeconds(long seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private String randomId() {
        return IdGenerator.generate();
    }

}
