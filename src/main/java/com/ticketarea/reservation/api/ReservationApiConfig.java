package com.ticketarea.reservation.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.any;

@ComponentScan("com.ticketarea.reservation.api")
@EnableJpaRepositories(basePackages = "com.ticketarea.reservation.api.infrastructure.jpa", considerNestedRepositories = true)
@EntityScan(basePackages = "com.ticketarea.reservation.api.application")
@EnableSwagger2
@EnableScheduling
@SpringBootApplication
public class ReservationApiConfig {

    public static void main(String... args) {
        SpringApplication.run(ReservationApiConfig.class, args);
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(getApiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.ticketarea.reservation.api.boundary"))
                .paths(any())
                .build();
    }

    private ApiInfo getApiInfo() {
        Contact contact = new Contact("Gerard Szczepanski", "https://craftinginjava.com/", "gerardszczepanski@gmail.com");
        return new ApiInfoBuilder()
                .title("Reservation API")
                .description("Api for reservations in Ticketarea reservation system")
                .version("1.0")
                .license("Apache 2.0")
                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0")
                .contact(contact)
                .build();
    }

}
