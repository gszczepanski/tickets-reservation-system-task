package com.ticketarea.reservation.api.infrastructure.jpa;

import com.ticketarea.reservation.api.application.ticket.SeatDetails;
import com.ticketarea.reservation.api.application.ticket.Ticket;
import com.ticketarea.reservation.api.application.ticket.TicketId;
import com.ticketarea.reservation.api.application.ticket.TicketRepository;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Component //TODO create real impl and remove stub
class TicketRepositoryStub implements TicketRepository {

    @Override
    public Optional<Ticket> findById(TicketId id) {
        return Optional.of(
                new Ticket(
                        new SeatDetails("A1", "2", "12")
                )
        );
    }

    @Override
    public Set<Ticket> findByIds(Set<TicketId> ticketsIds) {
        return new HashSet<>();
    }

}
