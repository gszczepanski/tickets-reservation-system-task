package com.ticketarea.reservation.api.infrastructure.registry;

import com.ticketarea.reservation.api.application.shared.Clock;
import com.ticketarea.reservation.api.application.shared.DomainRegistry;
import com.ticketarea.reservation.api.application.event.EventRepository;
import com.ticketarea.reservation.api.application.reservation.ReservationRepository;
import com.ticketarea.reservation.api.application.ticket.TicketRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
class DomainRegistryImpl implements ApplicationContextAware, DomainRegistry {

    private ApplicationContext context;

    public void setApplicationContext(ApplicationContext context) {
        this.context = context;
    }

    @Override
    public Clock clock() {
        return context.getBean(Clock.class);
    }

    @Override
    public EventRepository eventRepository() {
        return context.getBean(EventRepository.class);
    }

    @Override
    public ReservationRepository reservationRepository() {
        return context.getBean(ReservationRepository.class);
    }

    @Override
    public TicketRepository ticketRepository() {
        return context.getBean(TicketRepository.class);
    }
}
