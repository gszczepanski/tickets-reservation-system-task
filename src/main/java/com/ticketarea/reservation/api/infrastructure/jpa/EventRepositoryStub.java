package com.ticketarea.reservation.api.infrastructure.jpa;

import com.ticketarea.reservation.api.application.event.Event;
import com.ticketarea.reservation.api.application.event.EventId;
import com.ticketarea.reservation.api.application.event.EventRepository;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Optional;

@Component //TODO create real impl and remove stub
class EventRepositoryStub implements EventRepository {

    @Override
    public Optional<Event> findById(EventId id) {
        return Optional.of(
                new Event(id,
                        LocalDateTime.of(2018, 01, 01, 11, 00),
                        LocalDateTime.of(2019, 01, 01, 13, 00)
                )
        );
    }

}
