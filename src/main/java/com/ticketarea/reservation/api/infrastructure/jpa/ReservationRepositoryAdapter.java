package com.ticketarea.reservation.api.infrastructure.jpa;

import com.google.common.base.Preconditions;
import com.ticketarea.reservation.api.application.event.EventId;
import com.ticketarea.reservation.api.application.reservation.Reservation;
import com.ticketarea.reservation.api.application.reservation.Reservation.ReservationState;
import com.ticketarea.reservation.api.application.reservation.ReservationId;
import com.ticketarea.reservation.api.application.reservation.ReservationRepository;
import com.ticketarea.reservation.api.application.user.UserId;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.ticketarea.reservation.api.application.reservation.Reservation.ReservationState.CURRENT;

@Repository
@RequiredArgsConstructor
class ReservationRepositoryAdapter implements ReservationRepository {

    private final ReservationJpaRepository reservationJpaRepository;

    @Override
    public Optional<Reservation> findCurrentByUserIdAndEventId(UserId userId, EventId eventId) {
        return reservationJpaRepository.findCurrentReservationByIdAndEventId(userId, eventId, CURRENT);
    }

    @Override
    public Optional<Reservation> findById(ReservationId id) {
        checkNotNull(id);
        return reservationJpaRepository.findById(id.getValue());
    }

    @Override
    public Set<Reservation> findCurrentReservations() {
        return reservationJpaRepository.findReservationsWithState(CURRENT);
    }

    @Override
    public void save(Reservation reservation) {
        checkNotNull(reservation);
        reservationJpaRepository.save(reservation);
    }

    @Repository
    interface ReservationJpaRepository extends JpaRepository<Reservation, String> {

        @Query("SELECT r FROM Reservation r " +
                "WHERE r.userId = :userId " +
                "AND r.eventId = :eventId " +
                "AND r.state = :state")
        Optional<Reservation> findCurrentReservationByIdAndEventId(
                @Param("userId") UserId userId,
                @Param("eventId") EventId eventId,
                @Param("state") ReservationState state
        );

        @Query("SELECT r FROM Reservation r " +
                "WHERE r.state = :state")
        Set<Reservation> findReservationsWithState(@Param("state") ReservationState state);

        @Query("SELECT r FROM Reservation r WHERE r.id = :id")
        Optional<Reservation> findById(@Param("id") String id);

    }
}
