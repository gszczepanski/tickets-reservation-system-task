package com.ticketarea.reservation.api.boundary.reservation.controller;

import com.ticketarea.reservation.api.application.shared.Person;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;


@ToString
@ApiModel(description = "Request for add Ticket to Reservation (Reserve ticket)")
class AddTicketToReservationRequest {

    @Getter
    @ApiModelProperty(required = true, value = "DateTime as timestamp")
    private LocalDateTime dateTime;

    @ApiModelProperty(required = true, value = "Person assigned to ticket")
    private ReservationPerson person;

    public class ReservationPerson {

        @ApiModelProperty(required = true, value = "Person for ticket first name")
        private String firstName;

        @ApiModelProperty(required = true, value = "Person for ticket last name")
        private String lastName;

        @ApiModelProperty(required = true, value = "Date of birth for person")
        private LocalDate dateOfBirth;

    }

    public Person getPerson() {
        return new Person(
                person.firstName,
                person.lastName,
                person.dateOfBirth
        );
    }


}
