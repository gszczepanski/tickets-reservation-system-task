package com.ticketarea.reservation.api.boundary.reservation.controller;

import com.ticketarea.reservation.api.application.reservation.ReservationAlreadyExistException;
import com.ticketarea.reservation.api.application.reservation.ReservationId;
import com.ticketarea.reservation.api.application.reservation.ReservationService;
import com.ticketarea.reservation.api.application.reservation.UnableToCreateReservationForEventException;
import com.ticketarea.reservation.api.application.ticket.TicketId;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@RestController
@RequestMapping("/api/v1.0/reservation")
@Api(description = "Rest API for operations related to reservations", tags = "Reservation API")
@RequiredArgsConstructor
class ReservationController {

    private final ReservationService reservationService;

    @ApiOperation(value = "Create Reservation for user with given userId and eventId", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Reservation related to user and event is created. ReservationId of created Reservation is returned"),
            @ApiResponse(code = 409, message = "Reservation already exists"),
            @ApiResponse(code = 400, message = "Event with given eventId not found or event is not selling tickets")
    })
    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public String createReservation(@Valid @RequestBody CreateReservationRequest request) {
        log.info("Create Reservation request {}", request);

        ReservationId reservationId = reservationService.createReservation(request.getUserId(), request.getEventId());
        return reservationId.getValue();
    }

    @ApiOperation(value = "Add ticket to Reservation")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Ticket successfully added to Reservation"),
    })
    @PostMapping(value = "/{reservation-id}/ticket/{ticket-id}", consumes = APPLICATION_JSON_VALUE)
    @ResponseStatus(CREATED)
    public void addTicketToReservation(@PathVariable("reservation-id") ReservationId reservationId, @PathVariable("ticket-id") TicketId ticketId, @Valid @RequestBody AddTicketToReservationRequest request) {
        log.info("Add Ticket {} to Reservation {} with request {}", reservationId, ticketId, request);

        // TODO impl
    }

    @ResponseStatus(value = CONFLICT)
    @ExceptionHandler(ReservationAlreadyExistException.class)
    public void handleReservationAlreadyExist() {

    }

    @ResponseStatus(value = BAD_REQUEST)
    @ExceptionHandler({IllegalArgumentException.class, UnableToCreateReservationForEventException.class})
    public void handleBadRequest() {

    }

}
