package com.ticketarea.reservation.api.boundary.reservation.controller;

import com.ticketarea.reservation.api.application.event.EventId;
import com.ticketarea.reservation.api.application.user.UserId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;


@ToString
@AllArgsConstructor
@ApiModel(description = "Request for create Reservation related to user and event")
class CreateReservationRequest {

    @NotEmpty
    @ApiModelProperty(required = true, value = "Id of user")
    private final String userId;

    @NotEmpty
    @ApiModelProperty(required = true, value = "Id of event")
    private final String eventId;

    UserId getUserId() {
        return UserId.representationOf(userId);
    }

    EventId getEventId() {
        return EventId.representationOf(eventId);
    }


}
