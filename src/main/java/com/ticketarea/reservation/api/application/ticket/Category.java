package com.ticketarea.reservation.api.application.ticket;

import com.ticketarea.reservation.api.application.event.EventId;
import lombok.RequiredArgsConstructor;

/**
 * Represents ticket category created for event
 */
@RequiredArgsConstructor
public class Category {

    private final CategoryId id;
    private final EventId eventId;
    private String code;
    private TicketsNumberRestriction ticketsNumberRestriction;
    private Price price;
}
