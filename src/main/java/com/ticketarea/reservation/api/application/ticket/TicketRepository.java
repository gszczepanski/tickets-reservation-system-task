package com.ticketarea.reservation.api.application.ticket;

import java.util.Optional;
import java.util.Set;

public interface TicketRepository {

    Optional<Ticket> findById(TicketId id);

    Set<Ticket> findByIds(Set<TicketId> ticketsIds);

}
