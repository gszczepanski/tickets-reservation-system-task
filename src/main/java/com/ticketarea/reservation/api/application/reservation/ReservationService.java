package com.ticketarea.reservation.api.application.reservation;

import com.ticketarea.reservation.api.application.shared.DomainRegistry;
import com.ticketarea.reservation.api.application.event.EventId;
import com.ticketarea.reservation.api.application.shared.Person;
import com.ticketarea.reservation.api.application.ticket.TicketId;
import com.ticketarea.reservation.api.application.user.UserId;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@Transactional
@RequiredArgsConstructor
public class ReservationService {

    private final DomainRegistry domainRegistry;

    public ReservationId createReservation(UserId userId, EventId eventId) {
        Reservation reservation = Reservation.create(userId, eventId, domainRegistry);
        domainRegistry
                    .reservationRepository()
                    .save(reservation);

        return reservation.getId();
    }

    public void reserveTicket(ReserveTicketData data) {
        Reservation reservation = domainRegistry
                .reservationRepository()
                .findById(data.getReservationId())
                .orElseThrow(() -> new ReservationNotFoundException());

        reservation.reserveTicket(
                data.getTicketId(),
                data.getReservationTime(),
                data.getReservationPerson(),
                domainRegistry
        );
    }

    @Value
    static class ReserveTicketData {

        private final ReservationId reservationId;
        private final TicketId ticketId;
        private final ReservationPointInTime reservationTime;
        private final Person reservationPerson;

    }

}
