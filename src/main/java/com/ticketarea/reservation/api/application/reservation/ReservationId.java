package com.ticketarea.reservation.api.application.reservation;

import com.ticketarea.reservation.api.application.shared.IdGenerator;
import lombok.*;

import java.io.Serializable;

import static com.google.common.base.Preconditions.checkArgument;
import static lombok.AccessLevel.PRIVATE;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Value object that represents ReservationId
 */
@Value
@RequiredArgsConstructor(access = PRIVATE)
public class ReservationId implements Serializable {

    private final String value;

    public static ReservationId generate() {
        return new ReservationId(IdGenerator.generate());
    }

    public static ReservationId of(String value) {
        checkArgument(isNotBlank(value), "ReservationId is invalid");
        return new ReservationId(value);
    }


}
