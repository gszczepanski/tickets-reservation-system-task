package com.ticketarea.reservation.api.application.ticket;

import com.ticketarea.reservation.api.application.shared.IdGenerator;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import static com.google.common.base.Preconditions.checkArgument;
import static lombok.AccessLevel.PRIVATE;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Value object that represents id of the ticket.
 */
@Value
@RequiredArgsConstructor(access = PRIVATE)
public class TicketId {

    private final String value;

    public static TicketId generate() {
        return new TicketId(IdGenerator.generate());
    }

    public static TicketId of(String value) {
        checkArgument(isNotBlank(value), "TicketId is invalid");
        return new TicketId(value);
    }

}
