package com.ticketarea.reservation.api.application.event;

import java.util.Optional;

/**
 * Represents port for Event data source.
 */
public interface EventRepository {

    Optional<Event> findById(EventId id);

}
