package com.ticketarea.reservation.api.application.shared;

import lombok.Value;

import java.time.LocalDate;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Shared value object between Ticket context and Reservation context
 */
@Value
public class Person {

    private final String firstName;
    private final String lastName;
    private final LocalDate dateOfBirth;

    public Person(String firstName, String lastName, LocalDate dateOfBirth) {
        checkArgument(isNotBlank(firstName), "First name must be not blank!");
        checkArgument(isNotBlank(lastName), "Last name must be not blank!");
        checkNotNull(dateOfBirth, "Birth date must be present!");

        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
    }
}
