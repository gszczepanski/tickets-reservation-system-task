package com.ticketarea.reservation.api.application.reservation;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.ticketarea.reservation.api.application.shared.DomainRegistry;
import com.ticketarea.reservation.api.application.shared.IdGenerator;
import com.ticketarea.reservation.api.application.ticket.Ticket;
import com.ticketarea.reservation.api.application.ticket.TicketId;
import com.ticketarea.reservation.api.application.ticket.TicketIdConverter;
import com.ticketarea.reservation.api.application.user.UserId;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.google.common.base.Preconditions.checkState;
import static java.util.stream.Collectors.*;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.EAGER;
import static lombok.AccessLevel.PACKAGE;

/**
 * Represents set of reserved tickets
 */
@Entity
@Table(name = "RESERVED_TICKETS")
@EqualsAndHashCode
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ReservedTickets {

    private static final int MAXIMUM_TICKETS_NUMBER = 8;

    @Id
    private final String id;

    @OneToMany(fetch = EAGER, cascade = ALL, mappedBy = "parent")
    private final Set<ReservedTicket> reservedTickets;

    void addToReservedTickets(Ticket ticket, ReservationPointInTime reservationTime) {
        checkState(reservedTickets.size() <= MAXIMUM_TICKETS_NUMBER, "Reservation can contain maximum 8 tickets!");

        ReservedTicket reservedTicket = new ReservedTicket(
                this,
                ticket.getId(),
                reservationTime.getDateTime()
        );
        reservedTickets.add(reservedTicket);
    }

    void releaseAllTickets(DomainRegistry registry) {
        if (reservedTickets.isEmpty()) {
            return;
        }

        Set<TicketId> reservedTicketIds = reservedTickets
                .stream()
                .map(r -> r.ticketId)
                .collect(toSet());

        registry.ticketRepository()
                .findByIds(reservedTicketIds)
                .stream()
                .forEach(Ticket::release);

        reservedTickets.clear();
    }

    // Hibernate only
    ReservedTickets() {
        this.id = null;
        this.reservedTickets = null;
    }

    static ReservedTickets createForReservation() {
        return new ReservedTickets(
            IdGenerator.generate(),
            new HashSet<>()
        );
    }

    @Entity
    @Table(name = "RESERVED_TICKET")
    static class ReservedTicket {

        @Id
        private final String id = IdGenerator.generate();

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "RESERVED_TICKETS_ID")
        private final ReservedTickets parent;

        @Column(name = "TICKET_ID")
        @Convert(converter = TicketIdConverter.class)
        private final TicketId ticketId;

        @Column(name = "RESERVATION_DATE")
        private final LocalDateTime reservationDate;

        ReservedTicket(ReservedTickets parent, TicketId ticketId, LocalDateTime reservationDate) {
            this.parent = parent;
            this.ticketId = ticketId;
            this.reservationDate = reservationDate;
        }

        // Hibernate usage only
        ReservedTicket() {
            this.parent = null;
            this.ticketId = null;
            this.reservationDate = null;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            ReservedTicket that = (ReservedTicket) o;
            return Objects.equals(id, that.id) &&
                    Objects.equals(ticketId, that.ticketId) &&
                    Objects.equals(reservationDate, that.reservationDate);
        }

        @Override
        public int hashCode() {
            return Objects.hash(id, ticketId, reservationDate);
        }
    }

}
