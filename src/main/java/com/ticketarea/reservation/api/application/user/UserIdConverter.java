package com.ticketarea.reservation.api.application.user;

import javax.persistence.AttributeConverter;

public class UserIdConverter implements AttributeConverter<UserId, String> {

    @Override
    public String convertToDatabaseColumn(UserId id) {
        if (id == null) {
            return null;
        }
        return id.getValue();
    }

    @Override
    public UserId convertToEntityAttribute(String s) {
        if (s == null) {
            return null;
        }
        return UserId.representationOf(s);
    }
}

