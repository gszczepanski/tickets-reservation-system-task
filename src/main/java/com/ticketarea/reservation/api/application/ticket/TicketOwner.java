package com.ticketarea.reservation.api.application.ticket;

import com.ticketarea.reservation.api.application.shared.Person;
import lombok.Value;

import java.time.LocalDate;

import static com.google.common.base.Preconditions.checkNotNull;

@Value
class TicketOwner {

    private final String firstName;
    private final String lastName;
    private final LocalDate dateOfBirth;

    TicketOwner(Person person) {
        checkNotNull(person, "Person must be present!");

        this.firstName = person.getFirstName();
        this.lastName = person.getLastName();
        this.dateOfBirth = person.getDateOfBirth();
    }
}
