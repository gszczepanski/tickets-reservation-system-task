package com.ticketarea.reservation.api.application.ticket;

public class TicketOperationDeniedException extends RuntimeException {

    public TicketOperationDeniedException() {
    }

    public TicketOperationDeniedException(String message) {
        super(message);
    }

    public TicketOperationDeniedException(String message, Throwable cause) {
        super(message, cause);
    }

    public TicketOperationDeniedException(Throwable cause) {
        super(cause);
    }

    public TicketOperationDeniedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
