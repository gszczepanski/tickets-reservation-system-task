package com.ticketarea.reservation.api.application.ticket;

import com.ticketarea.reservation.api.application.user.UserId;
import lombok.Value;

import static com.google.common.base.Preconditions.checkNotNull;

@Value
class Ownership {

    private final UserId userId;
    private final TicketOwner owner;

    Ownership(UserId userId, TicketOwner owner) {
        checkNotNull(userId);
        checkNotNull(owner);

        this.userId = userId;
        this.owner = owner;
    }
}
