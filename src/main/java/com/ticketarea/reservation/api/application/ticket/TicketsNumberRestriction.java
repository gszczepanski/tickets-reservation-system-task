package com.ticketarea.reservation.api.application.ticket;

import lombok.RequiredArgsConstructor;

/**
 * Represents optional restriction - how many tickets with this category can be reserved by one Reservation
 */
@RequiredArgsConstructor
public class TicketsNumberRestriction {

    private final int maximumTicketsPerReservation;

}
