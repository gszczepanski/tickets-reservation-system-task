package com.ticketarea.reservation.api.application.ticket;

import lombok.Value;

import static com.google.common.base.Preconditions.checkArgument;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Value
public class SeatDetails {

    private final String sector;
    private final String row;
    private final String seat;

    public SeatDetails(String sector, String row, String seat) {
        checkArgument(isNotBlank(sector), "Sector must be defined!");
        checkArgument(isNotBlank(row), "Row must be defined!");
        checkArgument(isNotBlank(seat), "Seat must be defined!");
        this.sector = sector;
        this.seat = seat;
        this.row = row;
    }
}
