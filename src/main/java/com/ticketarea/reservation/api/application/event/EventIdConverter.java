package com.ticketarea.reservation.api.application.event;

import javax.persistence.AttributeConverter;

public class EventIdConverter implements AttributeConverter<EventId, String> {

    @Override
    public String convertToDatabaseColumn(EventId id) {
        if (id == null) {
            return null;
        }
        return id.getValue();
    }

    @Override
    public EventId convertToEntityAttribute(String s) {
        if (s == null) {
            return null;
        }
        return EventId.representationOf(s);
    }
}

