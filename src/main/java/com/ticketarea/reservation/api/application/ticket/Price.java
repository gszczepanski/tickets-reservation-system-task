package com.ticketarea.reservation.api.application.ticket;

import lombok.Value;
import org.joda.money.Money;

/**
 * Value object that represents Price per category (ticket)
 */
@Value
public class Price {

    private final Money money;

}
