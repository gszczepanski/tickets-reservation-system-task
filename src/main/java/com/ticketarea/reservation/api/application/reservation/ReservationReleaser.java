package com.ticketarea.reservation.api.application.reservation;

import com.ticketarea.reservation.api.application.shared.DomainRegistry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Represents service with CRON for release expired Reservations
 */
@Service
@RequiredArgsConstructor
@Slf4j
@Transactional
class ReservationReleaser {

    private final DomainRegistry domainRegistry;

    @Scheduled(cron = "0/30 * * * * *")
    public void releaseReservations() {
        log.info("Reservations release start");

        domainRegistry
                .reservationRepository()
                .findCurrentReservations()
                .forEach(r -> r.timeValidation(domainRegistry));
    }

}
