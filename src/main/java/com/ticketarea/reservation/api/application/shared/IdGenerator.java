package com.ticketarea.reservation.api.application.shared;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.UUID;

import static lombok.AccessLevel.*;

@NoArgsConstructor(access = PRIVATE)
public class IdGenerator {

    public static String generate() {
        return UUID.randomUUID()
                    .toString()
                    .replaceAll("-", "");
    }

}
