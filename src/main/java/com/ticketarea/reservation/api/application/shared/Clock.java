package com.ticketarea.reservation.api.application.shared;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * Represents system time.
 */
@Component
public class Clock {

    public LocalDateTime whatDateTimeIsIt() {
        return LocalDateTime.now();
    }

}
