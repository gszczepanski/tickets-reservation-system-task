package com.ticketarea.reservation.api.application.shared;

import com.ticketarea.reservation.api.application.event.EventRepository;
import com.ticketarea.reservation.api.application.reservation.ReservationRepository;
import com.ticketarea.reservation.api.application.ticket.TicketRepository;

/**
 * This DomainRegistry is created to help crucial domain objects to work with domain services/repositories.
 * Without this interface I would be injecting these domain services/repos into domain object methods directly to allow them perform some advanced operations.
 *
 * This approach will clean up domain objects methods -> their contracts will be cleaner.
 * However, domain services/repos from another packages must be public now which can be considered as bad practice.
 *
 * Well... there are no silver bullets :)
 *
 * This pattern was taken from Vaughn Vernon DDD book.
 */
public interface DomainRegistry {

    Clock clock();

    EventRepository eventRepository();

    ReservationRepository reservationRepository();

    TicketRepository ticketRepository();

}
