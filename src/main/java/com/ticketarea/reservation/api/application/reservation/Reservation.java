package com.ticketarea.reservation.api.application.reservation;

import com.ticketarea.reservation.api.application.event.Event;
import com.ticketarea.reservation.api.application.event.EventId;
import com.ticketarea.reservation.api.application.event.EventIdConverter;
import com.ticketarea.reservation.api.application.shared.DomainRegistry;
import com.ticketarea.reservation.api.application.shared.Person;
import com.ticketarea.reservation.api.application.ticket.Ticket;
import com.ticketarea.reservation.api.application.ticket.TicketId;
import com.ticketarea.reservation.api.application.user.UserId;
import com.ticketarea.reservation.api.application.user.UserIdConverter;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Value;

import javax.persistence.*;
import java.time.LocalDateTime;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static com.ticketarea.reservation.api.application.reservation.Reservation.ReservationState.*;
import static java.lang.String.format;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.EnumType.STRING;
import static lombok.AccessLevel.PACKAGE;

/**
 * Represents 10 minute long Reservation in system.
 */
@Entity
@Table(name = "RESERVATION")
@AllArgsConstructor(access = PACKAGE)
@EqualsAndHashCode
public class Reservation {

    @Id
    private final String id;

    @Getter
    @Column(name = "USER_ID")
    @Convert(converter = UserIdConverter.class)
    private final UserId userId;

    @Getter
    @Column(name = "EVENT_ID")
    @Convert(converter = EventIdConverter.class)
    private final EventId eventId;

    @Embedded
    private final ReservationDateTimeInfo dateTimeInfo;

    @Enumerated(STRING)
    @Column(name = "STATE")
    private ReservationState state;

    @OneToOne(cascade = ALL)
    @JoinColumn(name = "RESERVED_TICKETS_ID")
    private final ReservedTickets reservedTickets;

    @Version
    @Column(name = "VERSION")
    private int version;

    /**
     * Creates Reservation for user with given UserId and event with given EventId.
     * Reservation is valid for 10 minutes.
     * After that time will expire.
     */
    static Reservation create(UserId userId, EventId eventId, DomainRegistry registry) {
        checkNotNull(userId);
        checkNotNull(eventId);

        registry.reservationRepository()
                .findCurrentByUserIdAndEventId(userId, eventId)
                .ifPresent(r -> {
                    throw new ReservationAlreadyExistException();
                });

        Event event = registry.eventRepository()
                .findById(eventId)
                .orElseThrow(() -> new IllegalArgumentException(format("Event with given id %s not found", eventId)));

        LocalDateTime started = registry.clock().whatDateTimeIsIt();

        if (started.isBefore(event.getSalesStartDate()) || started.isAfter(event.getSalesEndDate())) {
            throw new UnableToCreateReservationForEventException();
        }

        return new Reservation(
                ReservationId.generate().getValue(),
                userId,
                eventId,
                ReservationDateTimeInfo.from(started, event.getSalesEndDate()),
                CURRENT,
                ReservedTickets.createForReservation(),
                0
        );
    }

    // FOR HIBERNATE
    Reservation() {
        this.id = null;
        this.userId = null;
        this.eventId = null;
        this.dateTimeInfo = null;
        this.reservedTickets = null;
    }

    ReservationId getId() {
        return ReservationId.of(id);
    }

    /**
     * Allowed only in CURRENT state.
     * <br><br>
     * This method checks whether Reservation expired or not.
     * If yes, then state of Reservation change to EXPIRED.
     * <br><br>
     * This method alsop checks whether Event sales ended.
     * If yes, then state of Reservation change to TERMINATED.
     */
    void timeValidation(DomainRegistry registry) {
        checkState(state == CURRENT, "Operation denied");
        LocalDateTime now = registry.clock().whatDateTimeIsIt();

        if (dateTimeInfo.expired(now)) {
            expiration(registry);
        } else if (dateTimeInfo.shouldBeTerminated(now)) {
            state = TERMINATED;
        }
    }

    void reserveTicket(TicketId ticketId, ReservationPointInTime reservationTime, Person person, DomainRegistry registry) {
        checkIfTicketCanBeReserved(ticketId, reservationTime, person, registry);

        Ticket ticket = registry.ticketRepository()
                .findById(ticketId)
                .orElseThrow(() -> new IllegalArgumentException(format("Ticket with given id %s not found", ticketId)));

        makeReservation(ticket, reservationTime, person);
    }

    private void checkIfTicketCanBeReserved(TicketId ticketId, ReservationPointInTime reservationTime, Person person, DomainRegistry registry) {
        timeValidation(registry);
        checkState(state == CURRENT, "Reservation can not be done. Reservation is not in current state");
        checkNotNull(ticketId, "Ticket id must be present!");
        checkNotNull(person, "Person must be not null");
        checkNotNull(reservationTime, "Reservation time must not be null!");
    }

    private void makeReservation(Ticket ticket, ReservationPointInTime reservationTime, Person person) {
        ticket.reserve(getUserId(), person);
        reservedTickets.addToReservedTickets(ticket, reservationTime);
    }

    private void expiration(DomainRegistry registry) {
        state = EXPIRED;
        reservedTickets.releaseAllTickets(registry);
    }

    /**
     * State of reservation
     * <br><br>
     * CURRENT - Already created. Represents period from creation to expiration<br>
     * TERMINATED - Reservation was in current state but event salesEndDate income<br>
     * EXPIRED - Reservation was not finished in some period. There were expiration.<br>
     * FINISHED - Reservation was finished - tickets has been bought.<br>
     */
    public enum ReservationState {

        CURRENT,
        TERMINATED,
        EXPIRED,
        FINISHED

    }

    @Value
    @Embeddable
    @AllArgsConstructor(access = PACKAGE)
    static class ReservationDateTimeInfo {

        private static final long RESERVATION_PERIOD_IN_MINUTES = 10L;

        @Column(name = "STARTED", nullable = false)
        private final LocalDateTime started;
        @Column(name = "EXPIRES", nullable = false)
        private final LocalDateTime expires;
        @Column(name = "EVENT_SALES_END", nullable = false)
        private final LocalDateTime eventSalesEnd;

        private static ReservationDateTimeInfo from(LocalDateTime startDateTime, LocalDateTime eventSalesEnd) {
            LocalDateTime expires = startDateTime.plusMinutes(RESERVATION_PERIOD_IN_MINUTES);
            return new ReservationDateTimeInfo(startDateTime, expires, eventSalesEnd);
        }

        private boolean expired(LocalDateTime now) {
            return now.isAfter(expires);
        }

        private boolean shouldBeTerminated(LocalDateTime now) {
            return now.isAfter(eventSalesEnd);
        }

        // FOR HIBERNATE
        ReservationDateTimeInfo() {
            this.started = null;
            this.expires = null;
            this.eventSalesEnd = null;
        }
    }
}
