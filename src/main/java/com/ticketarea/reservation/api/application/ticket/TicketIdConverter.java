package com.ticketarea.reservation.api.application.ticket;

import com.ticketarea.reservation.api.application.user.UserId;

import javax.persistence.AttributeConverter;

public class TicketIdConverter implements AttributeConverter<TicketId, String> {

    @Override
    public String convertToDatabaseColumn(TicketId id) {
        if (id == null) {
            return null;
        }
        return id.getValue();
    }

    @Override
    public TicketId convertToEntityAttribute(String s) {
        if (s == null) {
            return null;
        }
        return TicketId.of(s);
    }
}

