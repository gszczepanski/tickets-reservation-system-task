package com.ticketarea.reservation.api.application.ticket;

import com.ticketarea.reservation.api.application.shared.Person;
import com.ticketarea.reservation.api.application.user.UserId;
import lombok.Getter;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.ticketarea.reservation.api.application.ticket.Ticket.TicketState.FREE;
import static com.ticketarea.reservation.api.application.ticket.Ticket.TicketState.RESERVED;
import static java.lang.String.format;

@Getter
public class Ticket {

    private final TicketId id;
    private final SeatDetails details;
    private TicketState state;
    private Ownership ownership;

    public Ticket(SeatDetails details) {
        checkNotNull(details);
        id = TicketId.generate();
        this.details = details;
        this.state = FREE;
    }

    public void reserve(UserId userId, Person person) {
        checkNotNull(userId);
        checkNotNull(person);
        guardTicketInFreeState();

        state = RESERVED;
        ownership = new Ownership(
                userId,
                new TicketOwner(person)
        );
    }

    public void release() {
        guardTicketIsInReservedState();
        state = FREE;
        ownership = null;
    }

    private void guardTicketIsInReservedState() {
        guardInState(RESERVED);
    }

    private void guardTicketInFreeState() {
        guardInState(FREE);
    }

    public boolean isFree() {
        return state == FREE;
    }

    private void guardInState(TicketState expectedState) {
        if (state != expectedState) {
            throw new TicketOperationDeniedException(format("To perform this operation Ticket must be in %s state!", expectedState));
        }
    }

    /**
     * Represents state of the Ticket.
     */
    enum TicketState {

        FREE,
        RESERVED,
        SOLD;

    }
}
