package com.ticketarea.reservation.api.application.reservation;

import com.ticketarea.reservation.api.application.event.EventId;
import com.ticketarea.reservation.api.application.user.UserId;

import java.util.Optional;
import java.util.Set;

public interface ReservationRepository {

    Optional<Reservation> findCurrentByUserIdAndEventId(UserId userId, EventId eventId);

    Optional<Reservation> findById(ReservationId id);

    Set<Reservation> findCurrentReservations();

    void save(Reservation reservation);

}
