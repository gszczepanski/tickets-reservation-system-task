package com.ticketarea.reservation.api.application.event;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;

/**
 * Represents Event from another subsystem.
 *
 */
@Getter
@RequiredArgsConstructor
public class Event {

    private final EventId id;
    private final LocalDateTime salesStartDate;
    private final LocalDateTime salesEndDate;
}
