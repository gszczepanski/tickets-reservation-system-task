package com.ticketarea.reservation.api.application.event;

import com.ticketarea.reservation.api.application.user.UserId;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import static com.google.common.base.Preconditions.checkArgument;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Value object that represents id of the event from EVENT subsystem.
 */
@Value
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class EventId {

    private final String value;

    public static EventId representationOf(String value) {
        checkArgument(isNotBlank(value), "Value passed as eventId is invalid");
        return new EventId(value);
    }

}
