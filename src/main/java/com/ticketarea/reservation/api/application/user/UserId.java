package com.ticketarea.reservation.api.application.user;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import static com.google.common.base.Preconditions.checkArgument;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Value object that represents id of the user from Users subsystem.
 */
@Value
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class UserId {

    private final String value;

    public static UserId representationOf(String value) {
        checkArgument(isNotBlank(value), "Value passed as userId is invalid");
        return new UserId(value);
    }

}