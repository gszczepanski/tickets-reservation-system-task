package com.ticketarea.reservation.api.application.reservation;

import lombok.RequiredArgsConstructor;
import lombok.Value;

import java.time.LocalDateTime;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.time.LocalDateTime.now;
import static lombok.AccessLevel.PRIVATE;

/**
 * Value object that represents reservation point in time.
 * Point in time can not be future.
 */
@Value
@RequiredArgsConstructor(access = PRIVATE)
public class ReservationPointInTime {

    private final LocalDateTime dateTime;

    public static ReservationPointInTime happenedAt(LocalDateTime dateTime) {
        checkNotNull(dateTime);
        checkArgument(dateTime.isBefore(now()), "Date time passed must happen before the creation of point in time!");

        return new ReservationPointInTime(dateTime);
    }


}
